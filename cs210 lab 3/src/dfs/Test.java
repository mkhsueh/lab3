package dfs;

import common.Constants;

public class Test
{
    static int BlockSize = 2048;
    public static void main(String[] args){
        // input 2 should be 
        int converted = convertFreeBlockIndexToID(2);
//       System.out.println("testing convert(2) = 81" + " " + converted );
    }
    
    
   
    /**
     * converts an index of the dataBlock freelist to the physical block ID
     * 
     * @param in
     * @return
     */
    private static int convertFreeBlockIndexToID (int in)
    {
        return in +
               1 +
            (int) Math.ceil(  (Constants.NUM_INODES * Constants.INODE_SIZE / BlockSize) +
               (BlockSize + ((double)512) * Constants.FILE_BLOCKS) / BlockSize) ;
    }

}
