package dfs;

import java.io.IOException;
import common.Constants;
import common.DFileID;
import dblockcache.DBuffer;

public class DFSClientThread implements Runnable
{

    private byte[] dataToWrite;
    private DFS myDFS;
    public DFSClientThread(DFS in){
        myDFS = in;
    }
    public int intFile;
    
    @Override
    public void run ()
    {
        // create a file
        DFileID fileID = myDFS.createDFile();
 //       DFileID fileID = new DFileID(intFile);
//        System.out.println("--------file id " + fileID.getFileID() );
        // write to the file
       
//        System.out.println("///////////");
//        System.out.println(dataToWrite.length);
        myDFS.write(fileID, dataToWrite, 0, dataToWrite.length);
        // read back the file
       // myDSF.sync();
        byte[] newArray = new byte[Constants.BLOCK_SIZE];
        for (int i = 0; i < 10; i++)
        {
            newArray[i] = dataToWrite[i];
        }

        
        byte[] readData = new byte[20];
        
//        try
//        {
//            Thread.sleep(2000);
//        }
//        catch (InterruptedException e)
//        {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        myDFS.read(fileID, readData, 0, 20);
        // print out the read data
 //       System.out.println("-----read result----------");
//        for (byte b : readData)
//        {
//  //          System.out.println(b + " ");
//        }
 //       myDSF.sync();
        
    }

    public byte[] getMyDataToWrite ()
    {
        return dataToWrite;
    }

    public void setMyDataToWrite (byte[] myDataToWrite)
    {
        this.dataToWrite = myDataToWrite;
    }

}
