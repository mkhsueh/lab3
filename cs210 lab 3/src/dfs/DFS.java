package dfs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import virtualdisk.VirtualDisk;
import common.Constants;
import common.DFileID;
import common.Inode;
import dblockcache.DBuffer;
import dblockcache.DBufferCache;


public class DFS
{

    private boolean _format;
    private String _volName;
    private VirtualDisk myDisk;
    private List<Inode> inodes = new ArrayList<Inode>();
    private List<Boolean> dataFreeList = new ArrayList<Boolean>(); //initalized from parsing
    private List<Boolean> inodeFreeList = new ArrayList<Boolean>(); //initalized from parsing
    private int totalBlocks;
    private int partitionRatio;
    private int inodeSize;
    private int diskBlockSize; // size of a single block
    public DBufferCache myCache;


    public DFS (String volName, boolean format)
    {
        _volName = volName;
        _format = format;
        try
        {
            //           //System.out.println("about to make vdf");
            myDisk = new VirtualDisk(_volName, _format);
            // if formatted, write in the superblock to VDF
            // adjust DBuffer cache size as needed
            int cacheSize = 512;
//            //System.out.println("made vdf");
            myCache = new DBufferCache(cacheSize);
//            //System.out.println("made cache");
            myCache.setDisk(myDisk);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        parseVDF();
    }


    public DFS (boolean format)
    {
        this(Constants.vdiskName, format);
    }


    public DFS ()
    {
        this(Constants.vdiskName, false);
    }


    public VirtualDisk getDisk ()
    {
        return this.myDisk;
    }


    /**
     * Parses the VDF, initializing inode list and freelist in DFS
     */
    private void parseVDF ()
    {
        File file = new File(Constants.vdiskName);
        FileInputStream fis;
        try
        {
            fis = new FileInputStream(file);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        byte[] bytes = new byte[(int) file.length()];

        ByteBuffer bb = ByteBuffer.wrap(bytes);

        // SUPERBLOCK

        // # block total (disk size)
        totalBlocks = bb.getInt();
        // partition ratio (we will use 1:10)
        partitionRatio = bb.getInt();
        // inode byte size
        inodeSize = bb.getInt();
        // disk block byte size
        diskBlockSize = bb.getInt();
        // advance the buffer to the freelist location
        bb.position(diskBlockSize);

        // FREELIST
        int x = Constants.NUM_INODES * Constants.FILE_BLOCKS;
        int zeroFillBytes =
                (int) Math.ceil((double) x / Constants.BLOCK_SIZE) *
                Constants.BLOCK_SIZE;
        for (int i = 0; i < Constants.BLOCK_SIZE + zeroFillBytes; i++)
        {
            byte temp = bb.get();
            /* adding to inodes freelist */
            if (i < 512)
            {
                if (temp == 0)
                {
                    inodeFreeList.add(true); // empty block
                }
                else
                {
                    inodeFreeList.add(false); // non-empty block
                }
            }
            /* adding to data blocks freelist */

            else if (i >= Constants.BLOCK_SIZE && i < Constants.BLOCK_SIZE + x)
            {
                if (temp == 0)
                {
                    dataFreeList.add(true); // empty block
                }
                else
                {
                    dataFreeList.add(false); // non-empty block
                }
            }

        }

        //INODES
        // parse inode list
        for (int i = 0; i < Constants.NUM_INODES; i++)
        {
            int tempInodeID = bb.getInt();
            // if no file id, node is blank--> skip
            // jump 256 bytes
            // continue; 
            if (tempInodeID == 0)
            {
                bb.position(inodeSize - 1 + bb.position()); //increment BB position by 1 inodeblock
                continue;
            }
            int tempInodeSize = bb.getInt();
            List<Integer> blockPointers = new ArrayList<Integer>();
            // loop through and get the block pointers
            for (int j = 0; j < 64 - 2; j++) //NOTE goto next granularity of inodes
            {
                Integer tempPointer = bb.getInt();
                if (tempPointer != 0)
                {
                    // only store non zero bytes
                    blockPointers.add(tempPointer);
                }
            }
            // construct the inode here and add to DFS Inode list
            Inode tempInode =
                    new Inode(tempInodeSize, tempInodeID, blockPointers);
            inodes.add(tempInode);
        }
//        //System.out.println("inodes size= " + this.inodes.size());

    }


    private void modifyInodeFreelist (int index, boolean value)
    {
        synchronized (this.inodeFreeList)
        {
            inodeFreeList.set(index, value);

        }
    }


    private void modifyDataFreelist (int index, boolean value)
    {
        synchronized (this.dataFreeList)
        {
            dataFreeList.set(index, value);

        }
    }


    //    /*
    //     * If format is true, the system should format the underlying disk contents,
    //     * i.e., initialize to empty. On success returns true, else return false.
    //     */
    //    public abstract boolean format ();

    /*
     * creates a new DFile and returns the DFileID, which is useful to uniquely
     * identify the DFile
     */
    public DFileID createDFile ()
    {
        /* create Dfile id */
        synchronized (inodeFreeList)
        {
            int newID = makeDFileID();
            //      synchronized (inodeFreeList)
            //      {
            //          freeIndex = inodeFreeList.indexOf(new Boolean(true));
            //      }
            /* check if there is space for a new inode */
            if (newID != -1)
            {
                /* if space, create new inode */
                Inode tempInode = new Inode(0, newID, new ArrayList<Integer>());
                //   tempInode.setFileID(newID);
                /* add new inode to inode list */
                inodes.add(tempInode);
                /* update inode freelist */
                modifyInodeFreelist(newID, false);
                return new DFileID(newID);
            }
            else
            {
                return null;
            }
        }
    }


    private int makeDFileID ()
    {
        // find first non null inode
        for (int i = 0; i < inodeFreeList.size(); i++)
        {
            if (inodeFreeList.get(i))
            {
                return i;
            }
        }
        return -1;
        //      while (currentIDs.contains(newID))
        //      {
        //          newID = generateID();
        //      }
        //      return newID;
    }


    //
    //  /**
    //   * @return
    //   */
    //  private int generateID ()
    //  {
    //      int newID = (int) Math.floor(Math.random() * 10000);
    //      return newID;
    //  }

    /* destroys the file specified by the DFileID */
    public void destroyDFile (DFileID dFID)
    {
        //TODO
        Inode node = inodes.get(dFID.getFileID());
        synchronized (node)
        {
            // update the freelist
            List<Integer> blockIDs = node.getBlockIDs();
            for (Integer integer : blockIDs)
            {
                inodeFreeList.set(this.convertIDToFreeBlockIndex(integer), true);
            }
            // clear the inode by adding the inode index to the freelist
        }
    }


    /*
     * reads the file dfile named by DFileID into the buffer starting from the
     * buffer offset startOffset; at most count bytes are transferred
     */
    public int read (DFileID dFID, byte[] buffer, int startOffset, int count)
    {

        // find inode by id
        Inode inode = findInode(dFID.getFileID());
        // //System.out.println("############## read pointers " +
        //                   inode.getBlockIDs().get(0));
        synchronized (inode)
        {

            if (inode == null)
            {
                //               //System.out.println("file does not exist");
                return -1; // file doesn't exist
            }
            if (startOffset > buffer.length)
            {
                //              //System.out.println("invalid offset used to read");
                return -1;
            }
            // use data block pointers to read the blocks
            // get blocks
            List<Integer> blockPointers = inode.getBlockIDs();
            //          //System.out.println(">>>>>>> bp size" + blockPointers.size());
            int currentCount = 0;

            // read block into buffer

            for (int i = 0; i < blockPointers.size(); i++)
            {
                DBuffer dbuf = myCache.getBlock(blockPointers.get(i));
                //             //System.out.println("<<<<< reading  block #" + dbuf.getBlockID());
                //               //System.out.println(">>>>>" + dbuf.getBuffer().length);
                if (currentCount + Constants.BLOCK_SIZE > count)
                {
                    // read up to the limit and break
                    for (int j = 0; j < dbuf.getBuffer().length; j++)
                    {
                        //System.out.println(dbuf.getBuffer()[j]);
                    }
//                    //System.out.println("-----inserting into " +
//                                       (Constants.BLOCK_SIZE * i + startOffset) +
//                                       " for length " + (count - currentCount));
                    dbuf.read(buffer,
                              Constants.BLOCK_SIZE * i + startOffset,
                              count - currentCount);
                    clearAfterEOF(buffer, startOffset, count);
                    return 1;

                }

                dbuf.read(buffer,
                          Constants.BLOCK_SIZE * i + startOffset,
                          Constants.BLOCK_SIZE);

                currentCount += Constants.BLOCK_SIZE;
                // release dbuf
                dbuf.releaseHold();
            }
            clearAfterEOF(buffer, startOffset, count);
            return 1;
        }
    }


    /**
     * @param buffer
     * @param startOffset
     * @param count
     */
    private void clearAfterEOF (byte[] buffer, int startOffset, int count)
    {
        boolean startClearing = false;
        for (int j = startOffset; j < startOffset + count; j++)
        {

            //  //System.out.println("j " + buffer[j]);
            if (buffer[j] == Constants.EOF)
            {
                //               //System.out.println("found EOF: at byte " + j);

                startClearing = true;
            }
            if (startClearing)
            {
                buffer[j] = 0;
            }
        }
    }


    private Inode findInode (int id)
    {
        for (Inode i : inodes)
        {
            if (i.getFileID() == id)
            {
                return i;
            }

        }
        return null;
    }


    /*
     * writes to the file specified by DFileID from the buffer starting from the
     * buffer offset startOffsetl at most count bytes are transferred
     */
    public int write (DFileID dFID, byte[] buffer, int startOffset, int count)
    {
        Inode inode = findInode(dFID.getFileID());
        synchronized (inode)
        {
            byte[] adjustedBuffer = new byte[count + 1];
            // copy the buffer into the adjusted buffer
            for (int i = 0; i < count; i++)
            {

                adjustedBuffer[i] = buffer[startOffset + i];

            }
            // write in the eof
            //          //System.out.println("input buffer size = " + buffer.length);
            //           //System.out.println("adjusted buffer size = " +
            //                              adjustedBuffer.length);
            adjustedBuffer[count] = Constants.EOF;
            //bytes to be written increased by size of EOF
            int adjustedCount = count + 1;

            List<Integer> blockPointers = inode.getBlockIDs();
            int currentCount = 0; // the current number of bytes that have been written to file
            //   boolean eofWritten = false;

            int temp =
                    ((int) Math.ceil((double) (adjustedCount) /
                                     Constants.BLOCK_SIZE));
            //          //System.out.println("temp= " + temp);
            //           //System.out.println("blockpointer size = " + blockPointers.size());
            //smaller case - need to remove blockPointers and dataFreeList
            if (adjustedCount / Constants.BLOCK_SIZE < blockPointers.size())
            {
                //               //System.out.println("smaller case");
                boolean partialWrite = false;

                currentCount = 0;
                int startingBPSize = blockPointers.size();
                for (int i = 0; i < startingBPSize; i++)
                {
                    DBuffer dbuf = myCache.getBlock(blockPointers.get(i));
                    if (currentCount + Constants.BLOCK_SIZE > adjustedCount)
                    {
                        //partial write to block, need to update inode and freelist 
                        if (!partialWrite && (adjustedCount > currentCount))
                        {
                            partialWrite(dbuf,
                                         adjustedBuffer,
                                         i,
                                         startOffset,
                                         adjustedCount,
                                         currentCount);
                            partialWrite = true;
                            //  eofWritten = true;
                        }
                        else
                        {
                            // Current count == count; write the eof

                            // iterating past the last written block; remove subsequent blocks on partial write
                            modifyDataFreelist(blockPointers.get(i) -
                                               (1 + (Constants.NUM_INODES *
                                                       Constants.INODE_SIZE / Constants.BLOCK_SIZE) + (Constants.BLOCK_SIZE + 512 * 256) /
                                                       Constants.BLOCK_SIZE),
                                                       new Boolean(true));
                            blockPointers.remove(i);
                        }
                    }
                    // writing the entire block
                    else
                    {
                        dbuf.write(adjustedBuffer,
                                   Constants.BLOCK_SIZE * i + startOffset,
                                   Constants.BLOCK_SIZE);
                    }
                    currentCount += Constants.BLOCK_SIZE;
                    // release dbuf
                    dbuf.releaseHold();
                }
            }

            //bigger case - need to add blockPointers and dataFreeList

            else if (temp > blockPointers.size())
            {
                //              //System.out.println("bigger case");
                boolean extraWrite = false;
                int currentNumBlock = inode.getNumBlocks();
                currentCount = 0;
                for (int i = 0; (double) i < (double) Math.ceil((double) adjustedCount /
                                                                Constants.BLOCK_SIZE); i++)
                {
                    DBuffer dbuf;
                    int freeBlockIndex;
                    int physID;
                    freeBlockIndex = this.dataFreeList.indexOf(true);
                    //                  //System.out.println("-------------DFS freeblockindex " +
                    //                                     freeBlockIndex);
                    physID = this.convertFreeBlockIndexToID(freeBlockIndex);
                    //TODO update phys id for other cases?
                    if (!(i > blockPointers.size() - 1))
                    {
                        dbuf = myCache.getBlock(blockPointers.get(i));
                    }
                    else
                    {
                        //                      //System.out.println(">>>>>>>>>>fetching new block");
                        //TODO changed freeblock index to phys id!!
                        dbuf = myCache.getBlockNew(physID);
                        //                      //System.out.println(">>>>>>>>>>fetched new block " +
                        dbuf.getBlockID();
                    }
                    if (currentCount + Constants.BLOCK_SIZE > adjustedCount &&
                            (adjustedCount > currentCount))
                    {
                        //partial write to block
                        partialWrite(dbuf,
                                     adjustedBuffer,
                                     i,
                                     startOffset,
                                     adjustedCount,
                                     currentCount);

                        synchronized (dataFreeList)
                        {

                            // set that data block unavailable in datafreelist
                            dataFreeList.set(freeBlockIndex, new Boolean(false));
                        }
                        // add block to blockPointers
                        //TODO the 153 and 90 dont match
                        blockPointers.add(physID);
                        // blockPointers.add(90);
                        //                      //System.out.println("---------DSF  physid " + physID);
                    }
                    else
                    {
                        //more than original # blocks
                        if (currentCount > currentNumBlock *
                                Constants.BLOCK_SIZE)
                        {
                            // get a free data block
                            //int freeBlockIndex;
                            synchronized (dataFreeList)
                            {
                                freeBlockIndex =
                                        this.dataFreeList.indexOf(new Boolean(true));
                                // set that data block unavailable in datafreelist
                                dataFreeList.set(freeBlockIndex,
                                                 new Boolean(false));
                            }
                            // add block to blockPointers
                            physID = convertFreeBlockIndexToID(freeBlockIndex);
                            blockPointers.add(physID);
                            //                        //System.out.println(">>>>>block pointesr");
                            for (Integer integer : blockPointers)
                            {
//                                //System.out.print(integer + " ");
                            }
                            dbuf = myCache.getBlock(physID);

                        }
                        dbuf.write(adjustedBuffer,
                                   Constants.BLOCK_SIZE * i + startOffset,
                                   Constants.BLOCK_SIZE);
                    }
                    currentCount += Constants.BLOCK_SIZE;
                    // release dbuf
                    dbuf.releaseHold();
                }
            }

            //same case
            else
            {
                boolean partialWrite = false;
                for (int i = 0; i < blockPointers.size(); i++)
                {
                    DBuffer dbuf = myCache.getBlock(blockPointers.get(i));
                    if (currentCount + Constants.BLOCK_SIZE > adjustedCount)
                    {
                        //partial write to block, need to update inode and freelist 
                        if (!partialWrite)
                        {
                            partialWrite(dbuf,
                                         adjustedBuffer,
                                         i,
                                         startOffset,
                                         adjustedCount,
                                         currentCount);
                            partialWrite = true;
                        }
                    }
                    else
                    {
                        dbuf.write(adjustedBuffer,
                                   Constants.BLOCK_SIZE * i + startOffset,
                                   Constants.BLOCK_SIZE);
                    }
                    currentCount += Constants.BLOCK_SIZE;
                    // release dbuf
                    dbuf.releaseHold();
                }
            }
            return 1;
        }
    }


    /**
     * converts an index of the dataBlock freelist to the physical block ID
     * 
     * @param in
     * @return
     */
    private int convertFreeBlockIndexToID (int in)
    {//TODO fix this
        int i =
                in +
                0 +
                (int) Math.ceil((Constants.NUM_INODES *
                        Constants.INODE_SIZE / Constants.BLOCK_SIZE) +
                        (Constants.BLOCK_SIZE + ((double) 512) *
                                Constants.FILE_BLOCKS) /
                                Constants.BLOCK_SIZE);
        //     //System.out.println("--------------convertFreeBlockIndexToID-" + i);
        return i;

    }


    private int convertIDToFreeBlockIndex (int in)
    {
        int i =
                in -

                (int) Math.ceil((Constants.NUM_INODES *
                        Constants.INODE_SIZE / Constants.BLOCK_SIZE) +
                        (Constants.BLOCK_SIZE + ((double) 512) *
                                Constants.FILE_BLOCKS) /
                                Constants.BLOCK_SIZE);
        //       //System.out.println("--------------convertIDToFreeBlockIndex-" + i);
        return i;

    }


    /**
     * writes the remaining bytes, given the count and current count
     * 
     * @param dbuf
     * @param buffer
     * @param iter
     * @param startOffset
     * @param count
     * @param currentCount
     */
    private void partialWrite (DBuffer dbuf,
                               byte[] buffer,
                               int iter,
                               int startOffset,
                               int count,
                               int currentCount)
    {
        //     //System.out.println(">>>> offset=" +
        //                       (Constants.BLOCK_SIZE * iter + startOffset) +
        //                         " count= " + (count - currentCount));
        for (byte b : buffer)
        {
 //           //System.out.println(b + " ");
        }
        dbuf.write(buffer, Constants.BLOCK_SIZE * iter + startOffset, count -
                   currentCount);
        // write the EOF
        // byte[] eof = new byte[1];
        //  eof[0] = 3;
        //  dbuf.write(eof, 0, 1);
    }


    /* returns the size in bytes of the file indicated by DFileID. */
    public int sizeDFile (DFileID dFID)
    {
        for (Inode node : inodes)
        {
            if (node.getFileID() == dFID.getFileID())
            {
                return node.getNumBlocks();
            }
        }
        return -1;

    }


    /*
     * List all the existing DFileIDs in the volume
     */
    public List<DFileID> listAllDFiles ()
    {
        List<DFileID> returnIDs = new ArrayList<DFileID>();
        for (Inode node : inodes)

        {
            DFileID tempID = new DFileID(node.getFileID());
            returnIDs.add(tempID);
        }
        return returnIDs;
    }


    /**
     * syncs the freelist, inodes to the VDF
     */
    public void sync ()
    {
        // get and push all blocks for freelist and inodes
        // when all pushed, sync the cache

        // push inode freebits
        byte[] inodeBufferArray;

        inodeBufferArray = new byte[Constants.BLOCK_SIZE];

        for (int i = 0; i < Constants.BLOCK_SIZE; i++)
        {
            if (i < 512)
            {
                if (inodeFreeList.get(i) == true)
                {
                    inodeBufferArray[i] = 1;
                }
            }
            else
            {
                inodeBufferArray[i] = 0;
            }

        }

        // push data block freebits
        DBuffer inodeBuffer = myCache.getBlock(1);
        inodeBuffer.write(inodeBufferArray, 0, Constants.BLOCK_SIZE);

        // build free buffer for data blocks
        byte[] dataBufferArray;

        int blocksForDataFreelist =
                (int) Math.ceil(Constants.NUM_INODES * Constants.FILE_BLOCKS /
                                (double) Constants.BLOCK_SIZE);

        dataBufferArray =
                new byte[blocksForDataFreelist * Constants.BLOCK_SIZE];

        for (int i = 0; i < blocksForDataFreelist * Constants.BLOCK_SIZE; i++)
        {
            if (dataFreeList.get(i) == true)
            {
                dataBufferArray[i] = 1;
            }
            else
            {
                dataBufferArray[i] = 0;
            }

        }

        // push block to disk
        for (int i = 0; i < blocksForDataFreelist; i++)
        {
            // get a dbuffer and write to disk
            DBuffer tempBuff = myCache.getBlock(2 + i);
            tempBuff.write(dataBufferArray,
                           i * Constants.BLOCK_SIZE,
                           Constants.BLOCK_SIZE);
        }

        //write inodes to disk

        byte[] wholeInodeBuffer =
                new byte[Constants.NUM_INODES * Constants.INODE_SIZE];
        byte[] tempArray = new byte[4];
        for (int i = 0; i < inodes.size(); i++)
        {
            Inode tempInode = inodes.get(i);
            for (int j = 0; j < 64; j++)
            {
                if (j == 0)
                {
                    // fill file id
                    tempArray = createByteArrayFromInt(tempInode.getFileID());
                }
                else if (j == 1)
                {
                    tempArray =
                            createByteArrayFromInt(tempInode.getNumBlocks());
                }
                else if (j > 1 && j < 52)
                {
                    tempArray =
                            createByteArrayFromInt(tempInode.getBlockIDs().get(j));
                    if (tempArray == null)
                    {
                        tempArray = new byte[] { 0, 0, 0, 0 };
                    }
                }
                else
                {
                    tempArray = new byte[] { 0, 0, 0, 0 };
                }

                for (int k = 0; k < 4; k++)
                {
                    wholeInodeBuffer[i * 256 + j * 4 + k] = tempArray[k];
                }
            }
        }

        for (int i = 0; i < wholeInodeBuffer.length / Constants.BLOCK_SIZE; i++)
        {
            // get a dbuffer and write to disk
            DBuffer tempBuff = myCache.getBlock(2 + blocksForDataFreelist + i);
            tempBuff.write(wholeInodeBuffer,
                           i * Constants.BLOCK_SIZE,
                           Constants.BLOCK_SIZE);
        }

        myCache.sync();
    }


    private byte[] createByteArrayFromInt (int i)
    {
        byte[] mybyte = new byte[4];
        mybyte[0] = (byte) (i >> 24);
        mybyte[1] = (byte) ((i << 8) >> 24);
        mybyte[2] = (byte) ((i << 16) >> 24);
        mybyte[3] = (byte) ((i << 24) >> 24);
        return mybyte;
    }
}
