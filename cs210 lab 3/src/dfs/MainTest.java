package dfs;

import virtualdisk.VDFWorkerThread;
import virtualdisk.VirtualDisk;

public class MainTest
{


    public static void main(String[] args){
        // make dfs
        DFS dfs = new DFS(false);
        // make  vdf thread
        VDFWorkerThread vdfThread = new VDFWorkerThread(dfs.getDisk());

        //  make client thread
        byte[] dataToWrite = new byte[]{1,2,11,3,5,6,7,8,9,10};
        byte[] dataToWrite2 = new byte[]{1,1,1,1,1,1,1,1,1,1};
        DFSClientThread dfsClient = new DFSClientThread(dfs);
        DFSClientThread dfsClient2 = new DFSClientThread(dfs);
        dfsClient.setMyDataToWrite(dataToWrite);
        dfsClient2.setMyDataToWrite(dataToWrite2);
        clientReadThread testRead = new clientReadThread(dfs);
        clientReadThread testRead2 = new clientReadThread(dfs);
        testRead.fileRead=0;
        testRead2.fileRead=1;

        testRead.setMyDataToWrite(dataToWrite2);

        Thread t1 = new Thread(vdfThread);
        Thread t2 = new Thread(dfsClient);
        Thread t3 = new Thread(dfsClient2);
        Thread t4 = new Thread(testRead);
        Thread t5 = new Thread(testRead2);



        // start vdf thread
//        System.out.println("starting vdf worker");
        t1.start();
        // start client thread
//       System.out.println("starting dfs client");
        t2.start();

        try
        {
            Thread.sleep(600);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        t3.start();

        try
        {
            Thread.sleep(600);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        t4.start();

        try
        {
            Thread.sleep(1600);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        t5.start();
    }
}
