package dblockcache;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import virtualdisk.VirtualDisk;
import common.Constants;
import common.Constants.DiskOperationType;


public class DBufferCache
{

    private int _cacheSize;
    private int myCacheSize;
    private List<DBuffer> myBuffers = new ArrayList<DBuffer>();
    private VirtualDisk myDisk;
    private static Lock getBlockLock = new ReentrantLock();
    private static Condition getBlockCondition = getBlockLock.newCondition();

    private Object testlock = new Object();


    /*
     * Constructor: allocates a cacheSize number of cache blocks, each
     * containing BLOCK-size bytes data, in memory
     */
    public DBufferCache (int cacheSize)
    {
        _cacheSize = cacheSize * Constants.BLOCK_SIZE;
        myCacheSize = cacheSize;
    }


    public void setDisk (VirtualDisk otherDisk)
    {
        myDisk = otherDisk;
    }


    /*
     * Get buffer for block specified by blockID. The buffer is "held" until the
     * caller releases it. A held buffer cannot be evicted: its block ID cannot
     * change.
     */
    public synchronized DBuffer getBlock (int blockID)
    {
        synchronized (myBuffers)
        {
            // check that the block is not being represented by another buffer
            // if already in cache, return existing buffer
            for (DBuffer buf : myBuffers)
            {
//TODO why no cache hit
                //           //System.out.println("searching for: " + blockID + " return " + buf.getBlockID());
                if (buf.getBlockID() == blockID && !buf.isHeld())
                {
                    //               //System.out.println("''''''''''cache hit");
//                for (byte b : buf.getBuffer())
//                {
//                    //System.out.println(b + " ");
//                }
                    // assumed here that locks are on files-- there shouldnt be multiple references to block buffers

                    return buf;
                }
            }
            // else, start fetch
            DBuffer tempBuffer = new DBuffer();
            tempBuffer.setBlockID(blockID);
            //       //System.out.println("TBUFFER BLOCKID = " + blockID);
            try
            {
                //           //System.out.println("fetching from disk in cache");
                tempBuffer.startFetch(myDisk);
//            //System.out.println("started fetch");
            }
            catch (IllegalArgumentException e2)
            {
                e2.printStackTrace();
            }

            // wait for io to complete
            try
            {
                getBlockLock.lock();
                //       //System.out.println("waiting for io");
                getBlockCondition.await();
                //      //System.out.println("WOKEN UP");
                //      //System.out.println("done waiting for io");
                getBlockLock.unlock();

            }
            catch (InterruptedException e1)
            {
                e1.printStackTrace();
            }

//        for (byte b : tempBuffer.mydata)
//        {
//            //System.out.println("TBuffer data = " + b);
//        }
//        

            while (tempBuffer.isPinned() == true)
            {
                try
                {
                    getBlockLock.lock();
                    getBlockCondition.await();
                    //              //System.out.println("WOKEN UP");
                    getBlockLock.unlock();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }

//        for (byte b : tempBuffer.mydata)
//        {
//            //System.out.println("TBuffer data2 = " + b);
//        }

            // hold buffer
            tempBuffer.hold();

            // find and evict the LRU buffer if not enough space
            if (myBuffers.size() == myCacheSize)
            {
                int status = removeLRUBlock();
                while (status == -1)
                {
                    // wait and try again
                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e)
                    {
                        //                  //System.out.println("could not sleep thread in getBlock()");
                        e.printStackTrace();
                    }
                    status = removeLRUBlock();
                }
            }
            // add buffer to buffer list
            myBuffers.add(tempBuffer);
            //increment age
            incrementAge();
            // return buffer
            return tempBuffer;
        }

    }


    /*
     * Get buffer for block specified by blockID. The buffer is "held" until the
     * caller releases it. A held buffer cannot be evicted: its block ID cannot
     * change.
     */
    public synchronized DBuffer getBlockNew (int blockID)
    {
        // check that the block is not being represented by another buffer
        // if already in cache, return existing buffer
        synchronized (myBuffers)
        {
            for (DBuffer buf : myBuffers)
            {

                if (buf.getBlockID() == blockID)
                {
                    //              //System.out.println("''''''''''cache hit");
                    // assumed here that locks are on files-- there shouldnt be multiple references to block buffers

                    return buf;
                }
            }
            // only fetch if this block already exists in disk

            DBuffer tempBuffer = new DBuffer();
            tempBuffer.setBlockID(blockID);
            //       //System.out.println("---------set new buffer id: " + tempBuffer.getBlockID());

            // hold buffer
            tempBuffer.hold();

            // find and evict the LRU buffer if not enough space
            if (myBuffers.size() == myCacheSize)
            {
                //           //System.out.println("---------cache was full in get new block");
                int status = removeLRUBlock();
                while (status == -1)
                {
                    // wait and try again
                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e)
                    {
                        //                  //System.out.println("could not sleep thread in getBlock()");
                        e.printStackTrace();
                    }
                    status = removeLRUBlock();
                    //               //System.out.println("Status = " + status);
                }
            }
            // add buffer to buffer list
            myBuffers.add(tempBuffer);
            //increment age
            incrementAge();
            // return buffer
            //       //System.out.println("----------returning new buffer");

            return tempBuffer;
        }

    }


    /* Release the buffer so that others waiting on it can use it */
    public synchronized void releaseBlock (DBuffer buf)
    {
        buf.releaseHold();

    }


    /*
     * sync() writes back all dirty blocks to the volume and wait for
     * completion. The sync() method should maintain clean block copies in
     * DBufferCache.
     */
    public synchronized void sync ()
    {
        synchronized (myBuffers)
        {
            for (DBuffer dbuf : myBuffers)
            {

                dbuf.startPush(myDisk);

            }
        }
    }


    /**
     * Removes a block according to eviction policy. Removes the least recently
     * used block that is also not held. Returns error code -1 if no blocks can
     * be currently removed
     */
    private synchronized int removeLRUBlock ()
    {
        synchronized (myBuffers)
        {
            int currentMax = -1;
            DBuffer currentBuffer = null;
            for (DBuffer dbuf : myBuffers)
            {
                //          //System.out.println("held = " + dbuf.isHeld());
                //           //System.out.println("pinned = " + dbuf.isPinned());
                //          //System.out.println(dbuf.getAge());
                if (dbuf.getAge() > currentMax && (!dbuf.isHeld()) &&
                    !dbuf.isPinned())
                {
                    //              //System.out.println("removable buffer");
                    currentMax = dbuf.getAge();
                    currentBuffer = dbuf;
                }

            }
            if (currentBuffer != null)
            {
                // check dirty and push if necessary
                currentBuffer.startPush(myDisk);
                myBuffers.remove(currentBuffer);
            }
            else if (currentBuffer == null)
            {
                return -1;
            }
            return 0;
        }
    }


    public static void notifyIOCV ()
    {
        synchronized (getBlockCondition)
        {
            getBlockLock.lock();
            getBlockCondition.signalAll();
            getBlockLock.unlock();
            //           //System.out.println("NOTIFIED*****************");
        }
    }


    private void incrementAge ()
    {
        for (DBuffer buff : myBuffers)
        {
            buff.incrementAge();
        }
    }
}
