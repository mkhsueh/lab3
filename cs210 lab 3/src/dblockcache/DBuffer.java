package dblockcache;

import java.io.IOException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import virtualdisk.VirtualDisk;
import common.Constants;
import common.Constants.DiskOperationType;


public class DBuffer
{

    private int age = 0;
    private boolean isHeld = false;
    private boolean isPinned = false;
    public byte[] mydata = new byte[Constants.BLOCK_SIZE];
    private boolean isValid = false;
    private boolean isDirty = false; // true if dirty
    private int blockID;
    private static Lock getBlockLock = new ReentrantLock();
    private static Condition getBlockCondition = getBlockLock.newCondition();
    private boolean holdCVSignaled = false;


    public void setBlockID (int id)
    {
        blockID = id;
    }


    public int getAge ()
    {
        return age;
    }


    public void setValid (boolean in)
    {
        this.isValid = in;
    }


    // true for dirty
    public synchronized void setDirty (boolean status)
    {
        isDirty = status;
    }


    public synchronized void hold ()
    {
        isHeld = true;
    }


    public synchronized void releaseHold ()
    {
        isHeld = false;
    }


    // returns whether the DBuffer is currently held
    public boolean isHeld ()
    {
        return isHeld;
    }


    /* Start an asynchronous fetch of associated block from the volume */
    public void startFetch (VirtualDisk myDisk)
    {
        try
        {
            this.isPinned = true;
            myDisk.startRequest(this, DiskOperationType.READ);
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    /* Start an asynchronous write of buffer contents to block on volume */
    public void startPush (VirtualDisk myDisk)
    {
        if (this.isDirty)
        {
            try
            {
                this.isPinned = true;
                myDisk.startRequest(this, DiskOperationType.WRITE);
            }
            catch (IllegalArgumentException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }


    /*
     * Check whether the buffer is dirty, i.e., has modified data to be written
     * back. Returns 1 if dirty.
     */
    public boolean checkClean ()
    {
        return this.isDirty;
    }


    /*
     * Check if buffer is evictable: not evictable if I/O in progress, or buffer
     * is held
     */
    public boolean isBusy ()
    {
        return (!isPinned && !isHeld);
    };


    public boolean isPinned ()
    {
        return isPinned;
    }


    /*
     * reads into the buffer[] array from the contents of the DBuffer. Check
     * first that the DBuffer has a valid copy of the data! startOffset and
     * count are for the buffer array, not the DBuffer. Upon an error, it should
     * return -1, otherwise return number of bytes read.
     */
    public int read (byte[] buffer, int startOffset, int count)
    {
        //  check that dbuffer has valid copy of data
//        //System.out.println("DBuffer read count = " + count + " buffer size= " +
//                           buffer.length + " start offset=" + startOffset);
 //       //System.out.println("mydata size " + mydata.length);
        if (isPinned)
        {
            return -1;
        }
        try
        {
            for (int i = 0; i < count; i++)
            {

                buffer[i + startOffset] = mydata[i];

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return -1;
        }
        this.age = 0;

//        //System.out.println("--------dbuf read into user buf");
        for (byte b : buffer)
        {
 //           //System.out.print(b + " ");
        }
        return 0;
    }


    /*
     * writes into the DBuffer from the contents of buffer[] array. startOffset
     * and count are for the buffer array, not the DBuffer. Mark buffer dirty!
     * Upon an error, it should return -1, otherwise return number of bytes
     * written.
     */
    public int write (byte[] buffer, int startOffset, int count)
    {//mydata = new byte[count];
//        //System.out.println("____print buffer");
 //       //System.out.println("start offset= " + startOffset);
        if (mydata == null)
        {
 //           //System.out.println("mydata was null");
        }
        for (byte b : buffer)
        {
  //          //System.out.print(b + " ");
        }
        try
        {
            for (int i = 0; i < count; i++)
            {
 //               //System.out.println("______" + "writing index " + i);
                mydata[i] = buffer[i + startOffset];
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return -1;
        }
        this.isDirty = true;
        this.age = 0;
        return 0;
    }


    /*
     * An upcall from VirtualDisk layer to inform the completion of an IO
     * operation
     */
    public void ioComplete ()
    {
//        //System.out.println("----- IO complete upcalled in dbuffer");
        isPinned = false;
        DBufferCache.notifyIOCV();
//        for (int i = 0; i < 11; i++)
//        {
//            //System.out.println("BUFFER *********** " + mydata[i]);
//
//        }
    };


    /*
     * An upcall from VirtualDisk layer to fetch the blockID associated with a
     * startRequest operation
     */
    public int getBlockID ()
    {
        return blockID;
        // return 90;
    }


    /*
     * An upcall from VirtualDisk layer to fetch the buffer associated with
     * DBuffer object
     */
    public byte[] getBuffer ()
    {
        return mydata;
    }


    public synchronized void incrementAge ()
    {
        this.age++;
    }
}
