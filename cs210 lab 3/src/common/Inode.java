package common;

import java.util.ArrayList;
import java.util.List;


public class Inode
{
    private int numBlocks;
    private int fileID;
    private List<Integer> blockPointers = new ArrayList<Integer>();


    public Inode ()
    {

    }


    public void setFileID(int i){
        fileID = i;
    }
    public int getNumBlocks ()
    {
        return numBlocks;
    }


    public int getFileID ()
    {
        return fileID;
    }

    public Inode (int iNumBlocks, int ID)
    {
        numBlocks = iNumBlocks;
        fileID = ID;
        
        
    }

    public Inode (int iNumBlocks, int ID, List<Integer> nodes)
    {
        numBlocks = iNumBlocks;
        fileID = ID;
        blockPointers = nodes;
        
    }


    public List<Integer> getBlockIDs ()
    {
        return blockPointers;
    }

}
