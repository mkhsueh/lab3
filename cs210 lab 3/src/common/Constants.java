package common;

public class Constants {
	public static final int NUM_OF_BLOCKS = 16384; // 2^14
	public static final int BLOCK_SIZE = 1024; // 1kB
	public static final int INODE_SIZE = 256;
	public static final int NUM_INODES = 512;
	public static final byte EOF = -1;
	public static final int FILE_BLOCKS = 50;
	//TODO make sure we used the correct num blocks

	/* DStore Operation types */
	public enum DiskOperationType {
		READ, WRITE
	};

	/* Virtual disk file/store name */
	public static final String vdiskName = "DSTORE.dat";
}