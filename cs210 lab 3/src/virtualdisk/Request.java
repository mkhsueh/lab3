package virtualdisk;

import common.Constants.DiskOperationType;

import dblockcache.DBuffer;

public class Request {

    private DBuffer buf;
    private DiskOperationType operation;
    
    public Request(DBuffer myBuf, DiskOperationType myOperation){
        this.buf = myBuf;
        this.operation = myOperation;
    }
    
    public DBuffer getBuf(){
        return buf;
    }
    
    public DiskOperationType getOperation(){
        return operation;
    }
}
