package virtualdisk;

/**
 * this class processes requests in the VDF
 * @author mkh20
 *
 */
public class VDFWorkerThread implements Runnable
{

    private VirtualDisk myVDF;
    
    public VDFWorkerThread(VirtualDisk inVDF){
        myVDF = inVDF;
    }
    
    @Override
    public void run ()
    {
        myVDF.serviceRequests();
        
    }

    
    
}
